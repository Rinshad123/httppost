import 'package:flutter/material.dart';

import '../../../../Data/Models/homepage_model.dart';

class ListTileBuilder extends StatelessWidget {
  const ListTileBuilder(
    this.data, {
    Key? key,
  }) : super(key: key);

  final HomePageModel data;

  @override
  Widget build(BuildContext context) {
    return buildListTile();
  }

  ListTile buildListTile() {
    return ListTile(
    title: Text(
      data.name,
      style: const TextStyle(
          color: Colors.blue, fontSize: 40.00, fontWeight: FontWeight.bold),
    ),
    subtitle: Text(
      data.job,
      style: const TextStyle(
        color: Colors.grey,
        fontSize: 27.00,
      ),
    ),
    trailing: Text(
      "ID: ${data.id}",
      style: const TextStyle(
          color: Colors.red, fontSize: 27.00, fontWeight: FontWeight.bold),
    ),
  );
  }
}
