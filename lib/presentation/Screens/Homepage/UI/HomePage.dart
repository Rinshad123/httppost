import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../Bloc/homepage_bloc.dart';
import '../../../../Data/Models/homepage_model.dart';
import '../../../Components/error_screen.dart';
import '../../../Components/spacer.dart';

import '../../../Declarations/constants.dart';
import '../Widgets/listtile.dart';
import '../Widgets/textformfields.dart';


import '../Widgets/search_btn.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late FocusNode nameFocus;
  late FocusNode jobFocus;
  late FocusNode searchBtnFocus;
  late TextEditingController nameController;
  late TextEditingController jobController;

  @override
  void initState() {
    super.initState();
    nameFocus = FocusNode();
    jobFocus = FocusNode();
    searchBtnFocus = FocusNode();
    nameController = TextEditingController();
    jobController = TextEditingController();
  }

  @override
  void dispose() {
    nameFocus.dispose();
    jobFocus.dispose();
    searchBtnFocus.dispose();
    nameController.dispose();
    jobController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("POST request"),
      ),
      body: BlocConsumer<HomepageBloc, HomepageState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is HomepageLoaded) {
            return buildLoadedLayout(state.data);
          } else if (state is HomepageLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is HomepageError) {
            return const ErrorScreenBuilder();
          } else {
            return buildInitialLayout();
          }
        },
      ),
    );
  }

  Widget buildInitialLayout() => Center(
    child: buildColumn(),
  );

  Column buildColumn() {
    return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      buildInputField1(),
      HeightSpacer(myHeight: kSpacing),
      buildInputField2(),
      HeightSpacer(myHeight: kSpacing),
      buildSearchButtonBuilder(),
    ],
  );
  }

  SearchButtonBuilder buildSearchButtonBuilder() {
    return SearchButtonBuilder(
        focusNode: searchBtnFocus,
        name: nameController,
        job: jobController,
      );
  }

  InputField buildInputField2() {
    return InputField(
        focusNode: jobFocus,
        textController: jobController,
        label: "Designation",
        icons: const Icon(Icons.work, color: Colors.blue),
      );
  }

  InputField buildInputField1() {
    return InputField(
        focusNode: nameFocus,
        textController: nameController,
        label: "Name",
        icons: const Icon(
          Icons.person,
          color: Colors.blue,
        ),
      );
  }

  Widget buildLoadedLayout(HomePageModel data) => Center(
      child: Padding(
        padding: kHPadding * 2,
        child: ListTileBuilder(data),
      ));
}